# METEO ![Meteo](data/meteo.png  "Meteo")

### Know the forecast of the next hours & days.

Developed with Vala & Gtk, using OpenWeatherMap API (https://openweathermap.org/)

![Screenshot](./data/screens/screenshot_1.png  "Meteo")

### Features:

- Current weather, with information about temperature, pressure, wind speed and direction, sunrise & sunset.
- Forecast for next 18 hours.
- Forecast for next five days.
- Choose your units (metric, imperial or british).
- Choose your city, with maps help.
- Awesome maps with weather info.
- System tray indicator.



## How To Install


### Flatpak package:

<a href='https://flathub.org/apps/com.gitlab.bitseater.meteo'>
    <img width='200' alt='Download on Flathub' src='https://flathub.org/api/badge?locale=en'/>
  </a>

### Snap package

<a href="https://snapcraft.io/meteo">
  <img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

### For Debian and derivates:

You can download the last .deb package from:

[Package: Debian 12](https://gitlab.com/bitseater/meteo/-/jobs/artifacts/0.9.9.3/download?job=package%3Adebian)

											
### How To Build

Library Dependencies :

- gtk+-3.0
- libsoup-2.4
- json-glib-1.0
- webkit2gtk-4.0
- ayatana-appindicator3-0.1
- meson


For advanced users!

    git clone https://gitlab.com/bitseater/meteo.git
    cd meteo
    ./quick.sh -b

----

#### New on release 0.9.9.3:

    * Fixed some issues.
    * Add some languages

  Fixed issues: various.

----
### Other screenshots:

**A map with temperatures by Dark Sky**
![Screenshot](./data/screens/screenshot_2.png  "Meteo")

**About Meteo window:**
![Screenshot](./data/screens/screenshot_3.png  "Meteo")

**Indicator in panel / system tray:**
![Screenshot](./data/screens/screenshot_4.png  "Meteo")
