/*
* Copyright (c) 2017-2018 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Meteo.Widgets {

    public class Current : Gtk.Box {

        public Current (Meteo.MainWindow window, Meteo.Widgets.Header header) {
            orientation = Gtk.Orientation.HORIZONTAL;

            var setting = new Settings ("com.gitlab.bitseater.meteo");
            header.custom_title = null;
            header.set_title (setting.get_string ("location") + ", " + setting.get_string ("state") + " " + setting.get_string ("country"));
            header.change_visible (true);
            string idplace = setting.get_string ("idplace");
            var today = new Meteo.Widgets.Today (idplace, window);
            var forecast = new Meteo.Widgets.Forecast (idplace);
            var separator = new Gtk.Separator (Gtk.Orientation.VERTICAL);
            pack_start (today, true, true, 0);
            pack_start (separator, false, true, 0);
            pack_start (forecast, true, true, 0);

            //Configure header nav
            header.show_mapwindow.connect (() => {
                window.change_view (new Meteo.Widgets.MapView (window, header));
                window.show_all ();
            });

            //Update countdown
            var interval = setting.get_int ("interval");
            GLib.Timeout.add_seconds (interval, () => {
                header.upd_button.sensitive = true;
                var current = new Meteo.Widgets.Current (window, header);
                window.change_view (current);
                return false;
            }, GLib.Priority.DEFAULT);
        }
    }
}
